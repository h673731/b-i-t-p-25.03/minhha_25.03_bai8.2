package com.example.minhha_25._bai82;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {
    @Query("select student from StudentEntity student order by student.hoTen ASC")
    List<StudentEntity> getStudentByName();

    @Query("select student from StudentEntity student order by student.soDienThoai ASC")
    List<StudentEntity> getStudentByPhone();

    @Query("select student from StudentEntity student where student.hoTen like %:hoTen% and student.diaChi = :diaChi")
    List<StudentEntity> getStudentVanHaNoi(String hoTen, String diaChi);
}
