package com.example.minhha_25._bai82;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseException<T> {
    private T errorMessage;
    private Boolean status;
}
