package com.example.minhha_25._bai82;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentResponse {
    private Long id;
    private int maSV;
    private String hoTen;
    private String diaChi;
    private String soDienThoai;
}
