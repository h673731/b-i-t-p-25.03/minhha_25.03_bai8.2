package com.example.minhha_25._bai82;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public ResponseApi getStudentByName() {
        List<StudentEntity> studentEntityList = studentRepository.getStudentByName();
        List<StudentResponse> studentResponseList = studentEntityList
                .stream()
                .map(StudentMapping::mapEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseApi(studentResponseList, "Get success");
    }

    public ResponseApi getStudentByPhone() {
        List<StudentEntity> studentEntityList = studentRepository.getStudentByPhone();
        List<StudentResponse> studentResponseList = studentEntityList
                .stream()
                .map(StudentMapping::mapEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseApi(studentResponseList, "Get success");
    }

    public ResponseApi getStudentVanHaNoi(String hoTen, String diaChi) {
        List<StudentEntity> studentEntityList = studentRepository.getStudentVanHaNoi(hoTen, diaChi);
        List<StudentResponse> studentResponseList = studentEntityList
                .stream()
                .map(StudentMapping::mapEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseApi(studentResponseList, "Get success");
    }

    public ResponseApi addStudent(StudentRequest studentRequest) {
        StudentEntity studentEntity = StudentMapping.mapRequestToEntity(studentRequest);
        studentRepository.save(studentEntity);
        return new ResponseApi(null, "Add success");
    }

    public ResponseApi updateStudent(Long id, StudentRequest studentRequest) {
        StudentEntity studentEntity = StudentMapping.mapRequestToEntity(studentRequest);
        Optional<StudentEntity> studentEntityOptional = studentRepository.findById(id);
        studentEntityOptional.get().setMaSV(studentEntity.getMaSV());
        studentEntityOptional.get().setHoTen(studentEntity.getHoTen());
        studentEntityOptional.get().setDiaChi(studentEntity.getDiaChi());
        studentEntityOptional.get().setSoDienThoai(studentEntity.getSoDienThoai());
        studentRepository.save(studentEntityOptional.get());
        List<StudentResponse> studentResponseList = new ArrayList<>();
        StudentResponse studentResponse = StudentMapping.mapEntityToResponse(studentEntityOptional.get());
        studentResponseList.add(studentResponse);
        return new ResponseApi(studentResponseList, "Update success");
    }

    public ResponseApi deleteStudentById(Long id) {
        studentRepository.deleteById(id);
        return new ResponseApi(null, "Delete success");
    }

    public ResponseApi deleteAllStudent() {
        studentRepository.deleteAll();
        return new ResponseApi(null, "Delete success");
    }
}
