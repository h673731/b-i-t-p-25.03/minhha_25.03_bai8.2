package com.example.minhha_25._bai82;

import lombok.Data;

@Data
public class StudentMapping {
    public static StudentEntity mapRequestToEntity(StudentRequest studentRequest){
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setMaSV(studentRequest.getMaSV());
        studentEntity.setHoTen(studentRequest.getHoTen());
        studentEntity.setDiaChi(studentRequest.getDiaChi());
        studentEntity.setSoDienThoai(studentRequest.getSoDienThoai());
        return studentEntity;
    }

    public static StudentResponse mapEntityToResponse (StudentEntity studentEntity){
        StudentResponse studentResponse = new StudentResponse();
        studentResponse.setId(studentEntity.getId());
        studentResponse.setMaSV(studentEntity.getMaSV());
        studentResponse.setHoTen(studentEntity.getHoTen());
        studentResponse.setDiaChi(studentEntity.getDiaChi());
        studentResponse.setSoDienThoai(studentEntity.getSoDienThoai());
        return studentResponse;
    }
}
