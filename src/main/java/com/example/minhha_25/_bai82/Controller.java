package com.example.minhha_25._bai82;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/bai8.2")
public class Controller {
    @Autowired
    private StudentService studentService;
    @GetMapping("/get-student-by-name")
    public ResponseEntity<?> getStudentByName() {
        return new ResponseEntity<>(studentService.getStudentByName(), HttpStatus.OK);
    }

    @GetMapping("/get-student-by-phone")
    public ResponseEntity<?> getStudentByPhone() {
        return new ResponseEntity<>(studentService.getStudentByPhone(), HttpStatus.OK);
    }

    @GetMapping("/get-student-Van-HaNoi")
    public ResponseEntity<?> getStudentVanHaNoi(@RequestParam String hoTen, @RequestParam String diaChi) {
        return new ResponseEntity<>(studentService.getStudentVanHaNoi(hoTen, diaChi), HttpStatus.OK);
    }
    @PostMapping("/add-student")
    public ResponseEntity<?> addStudent(@Valid @RequestBody StudentRequest studentRequest) {
        return new ResponseEntity<>(studentService.addStudent(studentRequest), HttpStatus.OK);
    }
    @PutMapping("/update-student/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable Long id, @Valid @RequestBody StudentRequest studentRequest) {
        return new ResponseEntity<>(studentService.updateStudent(id, studentRequest), HttpStatus.OK);
    }
    @DeleteMapping("/delete-student/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable Long id) {
        return new ResponseEntity<>(studentService.deleteStudentById(id), HttpStatus.OK);
    }
    @DeleteMapping("/delete-all-student")
    public ResponseEntity<?> deleteAllStudent() {
        return new ResponseEntity<>(studentService.deleteAllStudent(), HttpStatus.OK);
    }
}
