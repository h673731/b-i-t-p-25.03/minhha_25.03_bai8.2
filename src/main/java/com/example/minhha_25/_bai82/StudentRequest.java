package com.example.minhha_25._bai82;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentRequest {
    @Min(value = 1, message = "Mã Sinh Viên không được để trống")
    private int maSV;
    @Size(min = 1, max = 255, message = "Tên Sinh Viên không được để trống")
    private String hoTen;
    @Size(min = 3, max = 250, message = "Địa chỉ không được để trống")
    private String diaChi;
    @Size(min = 7, max = 11, message = "Số điện thoại phải lớn hơn 7 số và nhỏ hơn 11 số")
    private String soDienThoai;
}
